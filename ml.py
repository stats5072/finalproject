#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 08:24:46 2024

@author: collinsullivan
"""

import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.metrics import mean_squared_error
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestRegressor
from xgboost import XGBRegressor
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import root_mean_squared_error
import os
import matplotlib.pyplot as plt
import seaborn as sns

def open_csv(relative_path,header):
    current_directory = os.getcwd()

    absolute_path = os.path.join(current_directory, relative_path)

    csv_data = pd.read_csv(absolute_path,header = header)

    return csv_data

# Replace 'full_df.csv' with the path to your CSV file if it's not in the current working directory
df = open_csv(relative_path="data/complete_df.csv",header=0)
filtered_df = df[(df['question'] == 'Fair or poor self-rated health status among adults') & (df['datavaluetype'] == 'Crude Prevalence')]

# Select relevant columns for the model
categorical_features = ['locationabbr', 'stratificationcategory1', 'stratification1']
target = 'datavalue'

# Apply OneHotEncoder to categorical features
preprocessor = ColumnTransformer(transformers=[
    ('onehot', OneHotEncoder(drop='first', sparse_output=False, handle_unknown= 'ignore'), categorical_features)
], remainder='passthrough')  


X = pd.merge(filtered_df[['yearstart','igini']], filtered_df[categorical_features], left_index=True, right_index=True)
y = filtered_df[target]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

## Model 1:
# Fit a linear regression model
linear_model = Pipeline(steps=[
    ('preprocessor', preprocessor),
    ('regressor', LinearRegression())
])
linear_model.fit(X_train, y_train)
linear_pred = linear_model.predict(X_test)

## Model 2:
# Fit a random forest model
rf_model = Pipeline(steps=[
    ('preprocessor', preprocessor),
    ('regressor', RandomForestRegressor(n_estimators=100, random_state=42))])
rf_model.fit(X_train, y_train)
rf_pred = rf_model.predict(X_test)

## Model 3:
# Fit an XGBoost model
xgb_model = Pipeline(steps=[
     ('preprocessor', preprocessor),
     ('regressor', XGBRegressor(n_estimators=100, random_state=42))])
xgb_model.fit(X_train, y_train)
xgb_pred = xgb_model.predict(X_test)

# Calculate mean squared error for each model
linear_mse = mean_squared_error(y_test, linear_pred)
rf_mse = mean_squared_error(y_test, rf_pred)
xgb_mse = mean_squared_error(y_test, xgb_pred)

# Calculate root mean squared error (RMSE) for each model
linear_rmse = root_mean_squared_error(y_test, linear_pred)
rf_rmse = root_mean_squared_error(y_test, rf_pred)
xgb_rmse = root_mean_squared_error(y_test, xgb_pred)

# Calculate mean absolute error (MAE) for each model
linear_mae = mean_absolute_error(y_test, linear_pred)
rf_mae = mean_absolute_error(y_test, rf_pred)
xgb_mae = mean_absolute_error(y_test, xgb_pred)

# Calculate the baseline errors for comparison
baseline_predictions = [y_train.mean()] * len(y_test)  # Using the mean value from the training set as the baseline
baseline_mse = mean_squared_error(y_test, baseline_predictions)
baseline_rmse = root_mean_squared_error(y_test, baseline_predictions)
baseline_mae = mean_absolute_error(y_test, baseline_predictions)

#Print the results
print(f"Linear regression MSE: {linear_mse}, RMSE: {linear_rmse}, MAE: {linear_mae}")
print(f"Random forest MSE: {rf_mse}, RMSE: {rf_rmse}, MAE: {rf_mae}")
print(f"XGBoost MSE: {xgb_mse}, RMSE: {xgb_rmse}, MAE: {xgb_mae}")
print(f"Baseline MSE: {baseline_mse}, RMSE: {baseline_rmse}, MAE: {baseline_mae}")


prediction_year = 2030
igini_fixed = 0.9
prediction_location = 'AL'
prediction_stratification = 'Sex'
strat_value = 'Female'
# Predict for the year X (assuming the features remain same as previous years)
X_predict = pd.DataFrame({
    'yearstart': [prediction_year],
    'igini': [igini_fixed],
    'locationabbr': [prediction_location],
    'stratificationcategory1': [prediction_stratification],
    'stratification1': [strat_value]
})
y_predict = xgb_model.predict(X_predict)

print(f"Predicted 'Fair or poor self-rated health status among adults %' for {prediction_year} in {prediction_location} for demographics {strat_value} with gini index {igini_fixed}: {y_predict[0]}")

import pickle

# Pickle linear model
filehandler_linear = open("linear_model.pkl", 'wb')
pickle.dump(xgb_model, filehandler_linear)

# Pickle random forest model
filehandler_rf = open("rf_model.pkl", 'wb')
pickle.dump(xgb_model, filehandler_rf)

# Pickle XGB model
filehandler_xgb = open("xgb_model.pkl", 'wb')
pickle.dump(xgb_model, filehandler_xgb)



# Plotting predicted vs. true values for XGBoost model
plt.figure(figsize=(10, 6))
sns.scatterplot(x=y_test, y=xgb_pred)
plt.plot([y_test.min(), y_test.max()], [y_test.min(), y_test.max()], 'm--', lw=2)
plt.title('Predicted vs. True Values for XGBoost Model', fontsize=16)
plt.xlabel('True Values', fontsize=14)
plt.ylabel('Predicted Values', fontsize=14)
plt.show()

#After our initial EDA analysis we decided on predicting the topic of self-reported health status based on variables State, Demographics and Income Enequality. We experimented with Percentage of Uninsured as another explaining variable, but it turned out that this variable doesn’t give any explanatory power as demonstrated by our EDA as well. Firstly we tried configured our Baseline Scenario, which was based only on mean of training data to give predictions. Our first Linear Regression Model did much better than our Baseline Scenario with MSE = 11.18, RMSE = 3.34 and MAE = 2.49. For comparison we trained also Random Forest model, which managed to score with similar results as Linear Regression, but after including income inequality index igini the results improved to MSE = 9.27, RMSE = 3.04 and MAE = 1.98. Finally we tried XGBoost model to predict our data and this model scored the best with final results of MSE = 7.88, RMSE = 2.80 and MAE = 1.63. It is fair to say, that the standard deviation of our target variable in our training dataset was 7.30, therefore RMSE of 2.80 and MAE of 1.63 certainly shows that our model is able to certain extent explain the variance of our data. Lastly we also plotted our XGBoost line to a curve and the results shows a good fit of our line through the data.