import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as pg
import cleaning

# loading data from cleaning.py
uninsured = cleaning.uninsured
healthtopics = cleaning.healthtopics
gini_index = cleaning.gini_index
healthtopics_states = cleaning.healthtopics_states

####
## Maps
####

# Uninsured map
uninsured_map = px.choropleth(uninsured, locations = 'sitsinstate',
                        locationmode = 'USA-states', color = 'ppopwoins', scope = 'usa',
                        title='Uninsured Population Rates by State',
                        color_continuous_scale='Cividis',
                        labels={
                            "ppopwoins": "Proportion of Population <br>without Insurance"
                        })

uninsured_map.update_layout(
                            plot_bgcolor='#22303c',
                            paper_bgcolor='#22303c',
                            geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                            font_color="white",
                            title_font_color="white",
                            legend_title_font_color="white"
                            )


# Self-reported health by states
health_trends_states = healthtopics[(healthtopics['locationabbr'] != 'US') &
                             (healthtopics['question'] == 'Fair or poor self-rated health status among adults') &
                             (healthtopics['datavaluetype'] == 'Crude Prevalence') &
                             (healthtopics['stratificationcategory1'] == 'Overall') &
                             (healthtopics['yearstart'] == 2022)]

health_trends_states_map = px.choropleth(health_trends_states, locations = 'locationabbr',
                        locationmode = 'USA-states', color = 'datavalue', scope = 'usa',
                        title='Prevalence of Adults with Fair/Poor Self-Rated Health Status in 2022',
                        color_continuous_scale='Cividis',
                        labels={
                            "datavalue": "Prevalence"
                        })

health_trends_states_map.update_layout(
                            plot_bgcolor='#22303c',
                            paper_bgcolor='#22303c',
                            geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                            font_color="white",
                            title_font_color="white",
                            legend_title_font_color="white"
                            )


# GINI Index map
gini_map = px.choropleth(gini_index, locations = 'sitsinstate',
                        locationmode = 'USA-states', color = 'igini', scope = 'usa',
                        title='GINI Index by State, 2018-2022',
                        color_continuous_scale='Cividis',
                        labels={
                            "igini": "GINI Index"
                        })

gini_map.update_layout(
                            plot_bgcolor='#22303c',
                            paper_bgcolor='#22303c',
                            geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                            font_color="white",
                            title_font_color="white",
                            legend_title_font_color="white"
                            )


# Unhealthy Days map
unhealthy_days = healthtopics[(healthtopics['question'] == 'Average recent physically unhealthy days among adults') &
                            (healthtopics['datavaluetype'] == 'Crude Mean') &
                            (healthtopics['stratificationcategory1'] == 'Overall') &
                            (healthtopics['yearstart'] == 2022)]

unhealthy_map = px.choropleth(unhealthy_days, locations = 'locationabbr',
                        locationmode = 'USA-states', color = 'datavalue', scope = 'usa',
                        title='Average Unhealthy Days by State in 2022',
                        color_continuous_scale='Cividis',
                        labels={
                            "datavalue": "Average Healthy Days <br> in last month"
                        })

unhealthy_map.update_layout(
                            plot_bgcolor='#22303c',
                            paper_bgcolor='#22303c',
                            geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                            font_color="white",
                            title_font_color="white",
                            legend_title_font_color="white"
                            )

# 2+ chronic conditions map
multi_chronic = healthtopics[(healthtopics['question'] == '2 or more chronic conditions among adults') &
                   (healthtopics['datavaluetype'] == 'Crude Prevalence') &
                   (healthtopics['stratificationcategory1'] == 'Overall') &
                   (healthtopics['yearstart'] == 2022)]

multi_chronic_map = px.choropleth(multi_chronic, locations = 'locationabbr',
                        locationmode = 'USA-states', color = 'datavalue', scope = 'usa',
                        title='Prevalence of 2+ Chronic Conditions Among Adults in 2022',
                        color_continuous_scale='Cividis',
                        labels={
                            "datavalue": "Percent of adults with <br> 2+ chronic health conditions"
                        })

multi_chronic_map.update_layout(
                                plot_bgcolor='#22303c',
                                paper_bgcolor='#22303c',
                                geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                                font_color="white",
                                title_font_color="white",
                                legend_title_font_color="white"
                                )

# Obesity map
obesity = healthtopics[(healthtopics['question'] == 'Obesity among adults') &
                   (healthtopics['datavaluetype'] == 'Crude Prevalence') &
                   (healthtopics['stratificationcategory1'] == 'Overall') &
                   (healthtopics['yearstart'] == 2022)]

obesity_map = px.choropleth(obesity, locations = 'locationabbr',
                        locationmode = 'USA-states', color = 'datavalue', scope = 'usa',
                        title='Prevalence of Obesity Among Adults in 2022',
                        color_continuous_scale='Cividis',
                        labels={
                            "datavalue": "Prevalence"
                        })

obesity_map.update_layout(
                            plot_bgcolor='#22303c',
                            paper_bgcolor='#22303c',
                            geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                            font_color="white",
                            title_font_color="white",
                            legend_title_font_color="white"
                            )

# BUBBLE CHART 

bubble = healthtopics_states[(healthtopics_states['question'] == 'Fair or poor self-rated health status among adults') &
                        (healthtopics_states['datavaluetype'] == 'Crude Prevalence') &
                        (healthtopics_states['stratificationcategory1'] == 'Overall') &
                        (healthtopics_states['yearstart'] == 2022)]

healthuninsured_bubble = px.scatter(bubble, x="datavalue", y="ppopwoins",
                                    size="igini", color="datavalue",hover_name="locationdesc",
                                    log_x=True, size_max=60,
                                    hover_data={"locationdesc": True, "datavalue": True, "ppopwoins": True, "igini": True})

healthuninsured_bubble.update_layout(xaxis_title='% Fair or poor self-rated health status among adults', yaxis_title='% Uninsured', 
                                     plot_bgcolor='#22303c',
                            paper_bgcolor='#22303c',
                            geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                            font_color="white",
                            title_font_color="white")




####
## Line charts
####

# Self-reported health by years
health_trends = healthtopics[(healthtopics['locationabbr'] == 'US') &
                             (healthtopics['question'] == 'Fair or poor self-rated health status among adults') &
                             # (healthtopics['datavaluetype'] == 'Crude Prevalence') &
                             (healthtopics['stratificationcategory1'] == 'Overall')]

fair_poor_custom_colors = ['#0097c3', '#f24f26']

health_trend_fig = px.line(health_trends, x='yearstart', y='datavalue', color='datavaluetype',
                    title='National Prevalence of Adults with Fair/Poor Self-Rated Health Status',
                    labels={'yearstart': 'Year', 'datavalue': 'Percent of Adults', 'datavaluetype': 'Data Value Type'},
                    color_discrete_sequence=fair_poor_custom_colors)
health_trend_fig.update_xaxes(type='linear', tick0=2000, dtick=1)

health_trend_fig.update_layout(
                                plot_bgcolor='#22303c',
                                paper_bgcolor='#22303c',
                                geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                                font_color="white",
                                title_font_color="white",
                                legend_title_font_color="white"
                                )


####
## Bar charts
####

## West Virginia vs US, 2019
wv_vs_us = healthtopics[(healthtopics['locationabbr'] == 'US') | (healthtopics['locationabbr'] == 'WV' )]

# Choose 2019 because that year had the most questions available for both US and WV
wv_vs_us_overall = wv_vs_us[(wv_vs_us['stratificationcategory1'] == 'Overall') &
                            (wv_vs_us['yearstart'] == 2019) &
                            (wv_vs_us['datavaluetype'] == 'Crude Prevalence')]

wv_us_custom_colors = ['#0097c3', '#f24f26']

# Comparing US and WV
wv_vs_us_comp = px.bar(wv_vs_us_overall, x='question', y='datavalue', color='locationabbr', barmode='group',
                         title='Comparing West Virginia to National Measures, 2019',
                         labels={'question': 'Question', 'locationabbr': 'Place', 'datavalue': 'Prevalence'},
                         color_discrete_sequence=wv_us_custom_colors)

wv_vs_us_comp.update_xaxes(tickangle=45)

wv_vs_us_comp.update_layout(
                                plot_bgcolor='#22303c',
                                paper_bgcolor='#22303c',
                                geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                                font_color="white",
                                title_font_color="white",
                                legend_title_font_color="white"
                                )

# same chart but dot plot
wv_vs_us_comp_dot = px.scatter(wv_vs_us_overall, y='datavalue', x='question', color='locationabbr', 
                 title='Comparing West Virginia to National Averages, 2019',
                labels={'question': 'Question', 'locationabbr': 'Place', 'datavalue': 'Prevalence'})

wv_vs_us_comp_dot.update_layout(
                            plot_bgcolor='#22303c',
                            paper_bgcolor='#22303c',
                            geo=dict(bgcolor= 'rgba(0,0,0,0)'), # transparent, I think
                            font_color="white",
                            title_font_color="white",
                            legend_title_font_color="white"
                            )


######
# WV vs US by race -- outcomes

wv_vs_us_byrace = wv_vs_us[(wv_vs_us['stratificationcategory1'] == 'Race/Ethnicity') &
                            (wv_vs_us['yearstart'] == 2022) &
                            (wv_vs_us['datavaluetype'] == 'Crude Prevalence')]

wv_vs_us_byrace_outcomes_questions = wv_vs_us_byrace[(wv_vs_us_byrace['question'] == 'Obesity among adults') |
                                            (wv_vs_us_byrace['question'] == '2 or more chronic conditions among adults') |
                                            (wv_vs_us_byrace['question'] == 'Fair or poor self-rated health status among adults')]

accent_color_palette = ['#64cb77', '#c1add7', '#ffbd7c', '#ffff89', '#206eb5', '#ff0080', '#ce5400', '#666666']
darker_color_palette = ['#00a173', '#ea5500', '#7670b8', '#fc008c', '#50a800', '#f1a800', '#206eb5', '#666666']


wv_vs_us_byrace_outcomes_figure = px.bar(wv_vs_us_byrace_outcomes_questions, x="stratification1", y="datavalue", color="stratification1", facet_col="question", facet_row="locationabbr",
                                title='Race and Health Outcomes, US vs West Virginia, 2022',
                                labels={'stratification1': 'Race', 'locationabbr': 'Place', 'datavalue': 'Prevalence'},
                                color_discrete_sequence=darker_color_palette)


wv_vs_us_byrace_outcomes_figure.update_xaxes(tickvals=[],
                                    title='')

names = {'question=2 or more chronic conditions among adults':'2 or more chronic <br> conditions among adults',
         'question=Obesity among adults':'Obesity Among Adults',
         'question=Fair or poor self-rated health status among adults':'Fair or poor self-rated <br> health status among adults',
         'Place=US': 'US',
         'Place=WV': 'WV',
         'Race': 'Race'}

wv_vs_us_byrace_outcomes_figure.for_each_annotation(lambda a: a.update(text = names[a.text]))

wv_vs_us_byrace_outcomes_figure.add_annotation(
    x=0.15,  # X-coordinate of the annotation (relative to the entire figure)
    y=-.1,  # Y-coordinate of the annotation (relative to the entire figure)
    xref='paper',  # Reference the entire plot area
    yref='paper',  # Reference the entire plot area
    text='Race',  # Text content of the annotation
    showarrow=False,  # Show an arrow pointing to the annotation
    font=dict(
        size=15  # Set the font size of the annotation text
    )
)

wv_vs_us_byrace_outcomes_figure.update_layout(
                                plot_bgcolor='#22303c',
                                paper_bgcolor='#22303c',
                                geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                                font_color="white",
                                title_font_color="white",
                                legend_title_font_color="white"
                                )

wv_vs_us_byrace_outcomes_figure.add_annotation(
    x=0.5,  # X-coordinate of the annotation (relative to the entire figure)
    y=-.1,  # Y-coordinate of the annotation (relative to the entire figure)
    xref='paper',  # Reference the entire plot area
    yref='paper',  # Reference the entire plot area
    text='Race',  # Text content of the annotation
    showarrow=False,  # Show an arrow pointing to the annotation
    font=dict(
        size=15  # Set the font size of the annotation text
    )
)

wv_vs_us_byrace_outcomes_figure.add_annotation(
    x=0.85,  # X-coordinate of the annotation (relative to the entire figure)
    y=-.1,  # Y-coordinate of the annotation (relative to the entire figure)
    xref='paper',  # Reference the entire plot area
    yref='paper',  # Reference the entire plot area
    text='Race',  # Text content of the annotation
    showarrow=False,  # Show an arrow pointing to the annotation
    font=dict(
        size=15  # Set the font size of the annotation text
    )
)

wv_vs_us_byrace_outcomes_figure.update_layout(
                                plot_bgcolor='#22303c',
                                paper_bgcolor='#22303c',
                                geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                                font_color="white",
                                title_font_color="white",
                                legend_title_font_color="white"
                                )


# WV vs US by race -- behavior
wv_vs_us_byrace = wv_vs_us[(wv_vs_us['stratificationcategory1'] == 'Race/Ethnicity') &
                            (wv_vs_us['yearstart'] == 2019) &
                            (wv_vs_us['datavaluetype'] == 'Crude Prevalence')]

wv_vs_us_byrace_behavior_questions = wv_vs_us_byrace[(wv_vs_us_byrace['question'] == 'Met aerobic physical activity guideline for substantial health benefits, adults') |
                                            (wv_vs_us_byrace['question'] == 'No leisure-time physical activity among adults') |
                                            (wv_vs_us_byrace['question'] == 'Consumed vegetables less than one time daily among adults')]

wv_vs_us_byrace_behavior_figure = px.bar(wv_vs_us_byrace_behavior_questions, x="stratification1", y="datavalue", color="stratification1", facet_col="question", facet_row="locationabbr",
                                title='Race and Health Behaviors, US vs West Virginia, 2019',
                                labels={'stratification1': 'Race', 'locationabbr': 'Place', 'datavalue': 'Prevalence'},
                                color_discrete_sequence=darker_color_palette)

wv_vs_us_byrace_behavior_figure.update_xaxes(tickvals=[],
                                             title='')

names = {'question=Met aerobic physical activity guideline for substantial health benefits, adults':'Met aerobic physical <br> activity guideline for substantial <br> health benefits, adults',
         'question=No leisure-time physical activity among adults':'No leisure-time physical <br> activity among adults',
         'question=Consumed vegetables less than one time daily among adults':'Consumed vegetables less than <br> one time daily among adults',
         'Place=US': 'US',
         'Place=WV': 'WV',
         'Race': 'Race'}

wv_vs_us_byrace_behavior_figure.for_each_annotation(lambda a: a.update(text = names[a.text]))

wv_vs_us_byrace_behavior_figure.add_annotation(
    x=0.15,  # X-coordinate of the annotation (relative to the entire figure)
    y=-.1,  # Y-coordinate of the annotation (relative to the entire figure)
    xref='paper',  # Reference the entire plot area
    yref='paper',  # Reference the entire plot area
    text='Race',  # Text content of the annotation
    showarrow=False,  # Show an arrow pointing to the annotation
    font=dict(
        size=15  # Set the font size of the annotation text
    )
)

wv_vs_us_byrace_behavior_figure.add_annotation(
    x=0.5,  # X-coordinate of the annotation (relative to the entire figure)
    y=-.1,  # Y-coordinate of the annotation (relative to the entire figure)
    xref='paper',  # Reference the entire plot area
    yref='paper',  # Reference the entire plot area
    text='Race',  # Text content of the annotation
    showarrow=False,  # Show an arrow pointing to the annotation
    font=dict(
        size=15  # Set the font size of the annotation text
    )
)

wv_vs_us_byrace_behavior_figure.add_annotation(
    x=0.85,  # X-coordinate of the annotation (relative to the entire figure)
    y=-.1,  # Y-coordinate of the annotation (relative to the entire figure)
    xref='paper',  # Reference the entire plot area
    yref='paper',  # Reference the entire plot area
    text='Race',  # Text content of the annotation
    showarrow=False,  # Show an arrow pointing to the annotation
    font=dict(
        size=15  # Set the font size of the annotation text
    )
)

wv_vs_us_byrace_behavior_figure.update_layout(
                            plot_bgcolor='#22303c',
                            paper_bgcolor='#22303c',
                            geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                            font_color="white",
                            title_font_color="white",
                            legend_title_font_color="white"
                            )


# WV vs US by sex/gender -- outcomes
wv_vs_us_outcomes_bysex = wv_vs_us[(wv_vs_us['stratificationcategory1'] == 'Sex') &
                            (wv_vs_us['yearstart'] == 2022) &
                            (wv_vs_us['datavaluetype'] == 'Crude Prevalence')]

wv_vs_us_outcomes_bysex_questions = wv_vs_us_outcomes_bysex[(wv_vs_us_outcomes_bysex['question'] == 'Obesity among adults') |
                                            (wv_vs_us_outcomes_bysex['question'] == '2 or more chronic conditions among adults') |
                                            (wv_vs_us_outcomes_bysex['question'] == 'Fair or poor self-rated health status among adults')]

green_purple = ['#7670b8', '#50a800']

# wv_vs_us_byrace_data = px.data.wv_vs_us_byrace()
wv_vs_us_outcomes_bysex_figure = px.bar(wv_vs_us_outcomes_bysex_questions, x="stratification1", y="datavalue", color="stratification1", facet_col="question", facet_row="locationabbr",
                                title='Sex/Gender and Health Outcomes, US vs West Virginia, 2022',
                                labels={'stratification1': 'Sex/Gender', 'locationabbr': 'Place', 'datavalue': 'Prevalence'},
                                color_discrete_sequence=green_purple)

# wv_vs_us_byrace_figure.update_xaxes(tickvals=[],
#                                     title='')

names = {'question=2 or more chronic conditions among adults':'2 or more chronic <br> conditions among adults',
         'question=Obesity among adults':'Obesity among adults',
         'question=Fair or poor self-rated health status among adults':'Fair or poor self-rated <br> health status among adults',
         'Place=US': 'US',
         'Place=WV': 'WV',
         'Sex': 'Sex'}

wv_vs_us_outcomes_bysex_figure.for_each_annotation(lambda a: a.update(text = names[a.text]))

wv_vs_us_outcomes_bysex_figure.update_layout(
                                            plot_bgcolor='#22303c',
                                            paper_bgcolor='#22303c',
                                            geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                                            font_color="white",
                                            title_font_color="white",
                                            legend_title_font_color="white"
                                            )


# WV vs US by sex/gender -- behavior
wv_vs_us_behavior_bysex = wv_vs_us[(wv_vs_us['stratificationcategory1'] == 'Sex') &
                            (wv_vs_us['yearstart'] == 2019) &
                            (wv_vs_us['datavaluetype'] == 'Crude Prevalence')]

wv_vs_us_behavior_bysex_questions = wv_vs_us_behavior_bysex[(wv_vs_us_outcomes_bysex['question'] == 'Met aerobic physical activity guideline for substantial health benefits, adults') |
                                            (wv_vs_us_behavior_bysex['question'] == 'No leisure-time physical activity among adults') |
                                            (wv_vs_us_behavior_bysex['question'] == 'Consumed vegetables less than one time daily among adults')]

wv_vs_us_behavior_bysex_figure = px.bar(wv_vs_us_behavior_bysex_questions, x="stratification1", y="datavalue", color="stratification1", facet_col="question", facet_row="locationabbr",
                                title='Sex/Gender and Health Behavior, US vs West Virginia, 2019',
                                labels={'stratification1': 'Sex/Gender', 'locationabbr': 'Place', 'datavalue': 'Prevalence'},
                                        color_discrete_sequence=green_purple)

names = {'question=Met aerobic physical activity guideline for substantial health benefits, adults':'Met aerobic physical activity guideline <br> for substantial health benefits, adults',
         'question=No leisure-time physical activity among adults':'No leisure-time physical <br> activity among adults',
         'question=Consumed vegetables less than one time daily among adults':'Consumed vegetables less than <br> one time daily among adults',
         'Place=US': 'US',
         'Place=WV': 'WV',
         'Sex/Gender': 'Sex/Gender'}

wv_vs_us_behavior_bysex_figure.for_each_annotation(lambda a: a.update(text = names[a.text]))

wv_vs_us_behavior_bysex_figure.update_layout(
                                plot_bgcolor='#22303c',
                                paper_bgcolor='#22303c',
                                geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                                font_color="white",
                                title_font_color="white",
                                legend_title_font_color="white"
                                )


# WV vs US by Age -- outcomes
wv_vs_us_outcomes_byage = wv_vs_us[(wv_vs_us['stratificationcategory1'] == 'Age') &
                            (wv_vs_us['yearstart'] == 2022) &
                            (wv_vs_us['datavaluetype'] == 'Crude Prevalence')]

wv_vs_us_outcomes_byage_questions = wv_vs_us_outcomes_byage[(wv_vs_us_outcomes_byage['question'] == 'Obesity among adults') |
                                            (wv_vs_us_outcomes_byage['question'] == '2 or more chronic conditions among adults') |
                                            (wv_vs_us_outcomes_byage['question'] == 'Fair or poor self-rated health status among adults')]

ordered_wv_vs_us_outcomes_byage_questions = wv_vs_us_outcomes_byage_questions.sort_values(
  by='stratification1', 
  ascending=True)

wv_vs_us_outcomes_byage_figure = px.bar(ordered_wv_vs_us_outcomes_byage_questions, x="stratification1", y="datavalue", color="stratification1", facet_col="question", facet_row="locationabbr",
                                title='Age and Health Outcomes, US vs West Virginia, 2022',
                                labels={'stratification1': 'Age', 'locationabbr': 'Place', 'datavalue': 'Prevalence'},
                                color_discrete_sequence=darker_color_palette)

names = {'question=2 or more chronic conditions among adults':'2 or more chronic <br> conditions among adults',
         'question=Obesity among adults':'Obesity among adults',
         'question=Fair or poor self-rated health status among adults':'Fair or poor self-rated <br> health status among adults',
         'Place=US': 'US',
         'Place=WV': 'WV',
         'Age': 'Age'}

wv_vs_us_outcomes_byage_figure.for_each_annotation(lambda a: a.update(text = names[a.text]))

wv_vs_us_outcomes_byage_figure.update_layout(
                                plot_bgcolor='#22303c',
                                paper_bgcolor='#22303c',
                                geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                                font_color="white",
                                title_font_color="white",
                                legend_title_font_color="white"
                                )


# WV vs US by age -- behavior
wv_vs_us_behavior_byage = wv_vs_us[(wv_vs_us['stratificationcategory1'] == 'Age') &
                            (wv_vs_us['yearstart'] == 2019) &
                            (wv_vs_us['datavaluetype'] == 'Crude Prevalence')]

wv_vs_us_behavior_byage_questions = wv_vs_us_behavior_byage[(wv_vs_us_behavior_byage['question'] == 'Met aerobic physical activity guideline for substantial health benefits, adults') |
                                            (wv_vs_us_behavior_byage['question'] == 'No leisure-time physical activity among adults') |
                                            (wv_vs_us_behavior_byage['question'] == 'Consumed vegetables less than one time daily among adults')]

ordered_wv_vs_us_behavior_byage_questions = wv_vs_us_behavior_byage_questions.sort_values(
  by='stratification1', 
  ascending=True)

wv_vs_us_behavior_byage_figure = px.bar(ordered_wv_vs_us_behavior_byage_questions, x="stratification1", y="datavalue", color="stratification1", facet_col="question", facet_row="locationabbr",
                                title='Age and Health Behavior, US vs West Virginia, 2019',
                                labels={'stratification1': 'Sex/Age', 'locationabbr': 'Place', 'datavalue': 'Prevalence'},
                                color_discrete_sequence=darker_color_palette)


names = {'question=Met aerobic physical activity guideline for substantial health benefits, adults':'Met aerobic physical activity guideline <br> for substantial health benefits, adults',
         'question=No leisure-time physical activity among adults':'No leisure-time physical <br> activity among adults',
         'question=Consumed vegetables less than one time daily among adults':'Consumed vegetables less than <br> one time daily among adults',
         'Place=US': 'US',
         'Place=WV': 'WV',
         'Age': 'Age'}

wv_vs_us_behavior_byage_figure.for_each_annotation(lambda a: a.update(text = names[a.text]))

wv_vs_us_behavior_byage_figure.update_layout(
                                plot_bgcolor='#22303c',
                                paper_bgcolor='#22303c',
                                geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                                font_color="white",
                                title_font_color="white",
                                legend_title_font_color="white"
                                )

# Self-reported health by race over time

health_trends_race = healthtopics[(healthtopics['locationabbr'] == 'US') &
                                    (healthtopics['question'] == 'Fair or poor self-rated health status among adults') &
                                    (healthtopics['datavaluetype'] == 'Crude Prevalence') &
                                    (healthtopics['stratificationcategory1'] == 'Race/Ethnicity')]

health_trends_race_fig = px.bar(health_trends_race, x='yearstart', y='datavalue', color='stratification1', barmode='group',
                         title='National prevalence of adults with fair/poor self-rated health status by race over time',
                         labels={'yearstart': 'Year', 'stratification1': 'Gender', 'datavalue': 'Percent of Adults'},
                         color_discrete_sequence=darker_color_palette)

health_trends_race_fig.update_layout(
                            plot_bgcolor='#22303c',
                            paper_bgcolor='#22303c',
                            geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                            font_color="white",
                            title_font_color="white",
                            legend_title_font_color="white"
                            )

# Self-reported health by gender over time

health_trends_gender = healthtopics[(healthtopics['locationabbr'] == 'US') &
                                    (healthtopics['question'] == 'Fair or poor self-rated health status among adults') &
                                    (healthtopics['datavaluetype'] == 'Crude Prevalence') &
                                    (healthtopics['stratificationcategory1'] == 'Sex')]

gender_year_fig = px.bar(health_trends_gender, x='yearstart', y='datavalue', color='stratification1',
                         title='National Prevalence of Adults with Fair/Poor Self-Rated Health Status by Gender Over Time',
                         labels={'yearstart': 'Year', 'stratification1': 'Gender', 'datavalue': 'Percent of Adults'},
                                color_discrete_sequence=green_purple)

gender_year_fig.update_xaxes(type='linear', tick0=2000, dtick=1)

gender_year_fig.update_layout(
                                plot_bgcolor='#22303c',
                                paper_bgcolor='#22303c',
                                geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                                font_color="white",
                                title_font_color="white",
                                legend_title_font_color="white"
                                )


# Self-reported health by race over time
health_trends_race = healthtopics[(healthtopics['locationabbr'] == 'US') &
                                    (healthtopics['question'] == 'Fair or poor self-rated health status among adults') &
                                    (healthtopics['datavaluetype'] == 'Crude Prevalence') &
                                    (healthtopics['stratificationcategory1'] == 'Race/Ethnicity')]

health_trends_race_fig = px.bar(health_trends_race, x='yearstart', y='datavalue', color='stratification1', barmode='group',
                         title='National Prevalence of Adults with Fair/Poor Self-Rated Health Status by Race Over Time',
                         labels={'yearstart': 'Year', 'stratification1': 'Gender', 'datavalue': 'Percent of Adults'},
                         color_discrete_sequence=darker_color_palette)

health_trends_race_fig.update_layout(
                                plot_bgcolor='#22303c',
                                paper_bgcolor='#22303c',
                                geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                                font_color="white",
                                title_font_color="white",
                                legend_title_font_color="white"
                                )



# Self-reported health by age over time
health_trends_age = healthtopics[(healthtopics['locationabbr'] == 'US') &
                                    (healthtopics['question'] == 'Fair or poor self-rated health status among adults') &
                                    (healthtopics['datavaluetype'] == 'Crude Prevalence') &
                                    (healthtopics['stratificationcategory1'] == 'Age')]

health_trends_age_fig = px.bar(health_trends_age, x='yearstart', y='datavalue', color='stratification1', barmode='group',
                         title='National Prevalence of Adults with Fair/Poor Self-Rated Health Status by Age Over Time',
                         labels={'yearstart': 'Year', 'stratification1': 'Age Group', 'datavalue': 'Percent of Adults'},
                         color_discrete_sequence=darker_color_palette)

health_trends_age_fig.update_layout(
                                plot_bgcolor='#22303c',
                                paper_bgcolor='#22303c',
                                geo=dict(bgcolor= 'rgba(0,0,0,0)', lakecolor='#22303c'),
                                font_color="white",
                                title_font_color="white",
                                legend_title_font_color="white"
                                )
