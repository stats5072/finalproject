import numpy as np
import pandas as pd
import dash
from dash.dependencies import Input, Output, State
from dash import dcc
from dash import html
from dash import callback
import plotly.express as px
import plotly.graph_objects as pg
import pandas as pd
import os
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.compose import ColumnTransformer
import pickle
import cleaning
import datavis

############
## Load the machine learning models from the pickle files
############

# Linear model
with open('linear_model.pkl', 'rb') as f:
    linear_model = pickle.load(f)

# Random Forest model
with open('rf_model.pkl', 'rb') as f:
    rf_model = pickle.load(f)

# XGBoost model
with open('xgb_model.pkl', 'rb') as f:
    xgb_model = pickle.load(f)


############
## Create the options for the machine learning dropdown menus
############

# Define options for dropdowns
years = np.arange(2023, 2076)
states = ['AK', 'AL', 'AR', 'AZ', 'CA', 'CO', 'CT', 'DC', 'DE', 'FL', 'GA',
          'HI', 'IA', 'ID', 'IL', 'IN', 'KS', 'KY', 'LA', 'MA', 'MD', 'ME',
          'MI', 'MN', 'MO', 'MS', 'MT', 'NC', 'ND', 'NE', 'NH', 'NJ', 'NM',
          'NV', 'NY', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX',
          'US', 'UT', 'VA', 'VT', 'WA', 'WI', 'WV', 'WY']

categories = [
    {'label': 'Overall', 'value': 'Overall'},
    {'label': 'Race/Ethnicity', 'value': 'Race/Ethnicity'},
    {'label': 'Sex', 'value': 'Sex'},
    {'label': 'Age', 'value': 'Age'}
]

# Define demographic subcategory options
age_options = [{'label': age, 'value': age} for age in ['Age 18-44', 'Age 45-64', 'Age >=65']]
sex_options = [{'label': sex, 'value': sex} for sex in ['Female', 'Male']]
race_ethnicity_options = [{'label': race, 'value': race} for race in [
    'Asian, non-Hispanic', 'Asian or Pacific Islander, non-Hispanic', 'Black, non-Hispanic', 
    'American Indian or Alaska Native, non-Hispanic', 'Hispanic', 'Hawaiian or Pacific Islander, non-Hispanic',
    'Multiracial, non-Hispanic', 'White, non-Hispanic'
]]

app = dash.Dash(__name__, suppress_callback_exceptions=True)
server = app.server

# Define model options for dropdown
model_options = [
    {'label': 'Linear Regression', 'value': 'linear'},
    {'label': 'Random Forest', 'value': 'random_forest'},
    {'label': 'XGBoost', 'value': 'xgboost'}
]









############
## Place the charts into the dashboard
############

# see https://plotly.com/python/px-arguments/ for more options
df_ex = pd.DataFrame({
    "Fruit": ["Apples", "Oranges", "Bananas", "Apples", "Oranges", "Bananas"],
    "Amount": [4, 1, 2, 2, 4, 5],
    "City": ["SF", "SF", "SF", "Montreal", "Montreal", "Montreal"]
})

fig = px.bar(df_ex, x="Fruit", y="Amount", color="City", barmode="group")

fig.update_layout(
    plot_bgcolor='#22303c',
    paper_bgcolor='#22303c',
    geo=dict(bgcolor= 'rgba(0,0,0,0)'), # transparent, I think
    font_color="white",
    title_font_color="white",
    legend_title_font_color="white"
)

df2 = pd.read_csv('https://gist.githubusercontent.com/chriddyp/5d1ea79569ed194d432e56108a04d188/raw/a9f9e8076b837d541398e999dcbac2b2826a81f8/gdp-life-exp-2007.csv')

fig2 = px.scatter(df2, x="gdp per capita", y="life expectancy",
                 size="population", color="continent", hover_name="country",
                 log_x=True, size_max=60)

fig2.update_layout(
    plot_bgcolor='#22303c',
    paper_bgcolor='#22303c',
    geo=dict(bgcolor= 'rgba(0,0,0,0)'), # transparent, I think
    font_color="white",
    title_font_color="white",
    legend_title_font_color="white"
)


# Theme:
THEME = {
    'bgcol'      : '#22303c',
    'hcolor'     : '#ff5418',
    'white'      : '#ffffff',
    'maize'      : '#F2C83F',
    'font'       : 'Open Sans',
    'font-size1' : 22,
}

# Tab styles and layout
# tabs_styles = {
#     'height': '500px'
# }
tab_style = {
    'padding': '35px',
    'font-family': 'Helvetica',
    'bacgroundColor': '#7f7f7f',
    'font-size': '110%',
    'padding': '35px',
    'margin': {'t': 0, 'b': 0, 'l': 20, 'r': 20}
}

tab_selected_style = {
    'padding': '35px',
    'font-family': 'Helvetica',
    'backgroundColor': '#22303c',
    'font-size': '110%',
    'border': '1px solid #ffffff',
    'color': '#F2C83F'
}


app.layout = html.Div([
    html.H1([
        html.Span('STATS 507 Final Project', style={'color': THEME['white']}),
        html.Span(' | ', style={'color': THEME['white']}),  # •
        html.Span('Maya Khuzam, Viktor Solansky, Collin Sullivan', style={'color': THEME['white']}),
    ],
    style={
        'font-family': 'Helvetica',
        'font-variant': 'small-caps',
        'font-weight': 'lighter',
        'font-size': '280%',
        'backgroundColor': THEME['bgcol']
    }), 
    
    html.Hr(style={'backgroundColor': THEME['bgcol']}), 

    html.Div('Exploring the social determinants of chronic diseases in the U.S.', 
            style={
                'color': THEME['maize'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '140%',
                'font-weight': 'Bold',
                "margin-left": "20px",
                "margin": "20px 0"
            }
        ),

    html.Div('For this project, we are interested in exploring the ways that variation in social and political phenomena relate to different chronic health outcomes.', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "40px",
                "margin": "20px"
            }
        ),

    html.Div('Although the Affordable Care Act of 2010 improved health coverage in the United States, nearly 1 in 13 U.S. adults (7.7%) is currently uninsured (Urban Institute 2024).', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "40px",
                "margin": "20px"
            }
        ),

    html.Div('Moreover, in its first four years of implementation, the ACA did not demonstrate an association with enhanced physical or general health among low-income adults (Griffith & Bor 2020). \
        However, rates of uninsurance and self-reported physical health vary widely by state.', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "40px",
                "margin": "20px"
            }
        ),

    html.Div('In 2022, the state of Texas had the highest uninsured rate of residents (17.58%), while simultaneously one-in-five (20.1%) of Texas residents reported having "fair or poor" health. \
On the other hand, West Virginia had an uninsured rate of roughly 6.39% while a stark one-in-four (25.4%) of its residents reported "fair or poor" health.', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "40px",
                "margin": "20px"
            }
        ),
    
    html.Div('This analysis seeks to answer the question: To what extent do social demographics explain health disparities in West Virginia despite seemingly low uninsured rates?', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "40px",
                "margin": "20px"
            }
        ),

    html.Div([
        html.Div([
            dcc.Tabs(id="tabs-all", value='', children=[
                dcc.Tab(label='National Trends', value='tab-1-national', style=tab_style, selected_style=tab_selected_style),
                dcc.Tab(label='Examining an Outlier: West Virginia', value='tab-2-wv', style=tab_style, selected_style=tab_selected_style),
                dcc.Tab(label='Predictions & Policy Implications', value='tab-3-pred', style=tab_style, selected_style=tab_selected_style),
                dcc.Tab(label='Methodology', value='tab-4-methods', style=tab_style, selected_style=tab_selected_style),
            ], 
            #style=tabs_styles,  
            vertical=True, 
            parent_style={'float': 'left'}
            ),
            html.Div(id='tabs-content', style={'float': 'left', 'width': '75%'})
        ])
    ])
]) 

@app.callback(Output('tabs-content', 'children'),
              [Input('tabs-all', 'value')])
def render_content(tab):
    if tab == 'tab-1-national':
        return html.Div([
            html.Div('National Trends', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '125%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px 0",
                'font-style': 'italic'
            }
        ),
        
        html.Div("Our analysis begins by looking at some high-level descriptive statistics related to health outcomes across the United States. We wanted to get a broad idea about the differences in health outcomes among different demographic groups, looking especially at age, race/ethnicity, and sex. We also wanted to look at overall outcomes at the state level, to see if any states stood out in such a way as to merit a closer look.", 
            style={
                'width': '85%',
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        ),

    # Line chart: Depression rates over time
    #dcc.Graph(
    #    id='dep_trend_fig',
    #    figure=datavis.dep_trend_fig,
    #    style={
    #        'width': '85%', 
    #        'border': '2px solid white', 
    #        'padding': '3px',
    #        "margin-left": "20px",
    #        "margin": "20px 0"}
   # ),

    # Line chart: self-reported health over time
    dcc.Graph(
        id='health_trend_fig',
        figure=datavis.health_trend_fig,
        style={
            'width': '85%', 
            'border': '2px solid white', 
            'padding': '3px',
            'margin': '20px'}
    ),
    
    html.Div('Here we see notable drops in both reported depression and reported \"bad days\" in 2020, the first year of the pandemic, and then climbing values afterward. The trends for both crude prevalence and age-adjusted prevalence are nearly exactly the same.', 
            style={
                'width': '85%', 
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        ),
    
    html.Div('We can also break down changes in self-reported health outcomes over time by race, sex, and age.', 
            style={
                'width': '85%', 
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        ),
    
    # Bar chart: Health trends by race
    dcc.Graph(
            id='health_trends_race_fig',
            figure=datavis.health_trends_race_fig,
            style={
                'width': '85%', 
                'border': '2px solid white', 
                'padding': '3px',
                'margin': '20px'}
        ),
    
    # Bar chart: Health trends by gender
    dcc.Graph(
            id='gender_year_fig',
            figure=datavis.gender_year_fig,
            style={
                'width': '85%', 
                'border': '2px solid white', 
                'padding': '3px',
                'margin': '20px'}
        ),
    
    # Bar chart: Health trends by age
    dcc.Graph(
            id='health_trends_age_fig',
            figure=datavis.health_trends_age_fig,
            style={
                'width': '85%', 
                'border': '2px solid white', 
                'padding': '3px',
                'margin': '20px'}
        ),
    
    html.Div('There appears to be a consistent drop in reported poor health outcomes in 2020, as demonstrated in the charts above, in which we see lower levels that year than any other year in the study for all groups. This may be a consequence of any of a number of things: during quarantine people spent less time commuting, so may have had more time for physical health; people were spending more time with family; and people may have been more likely that year to go for a run or walk in their neighborhood, just to get out of the house. This persistent dip in 2020 would be worth examining in more detail.', 
            style={
                'width': '85%', 
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        ),
    
    html.Div('Examining outcomes by state tells a slightly different story:', 
            style={
                'width': '85%', 
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        ),

    # Map: Health trends (Fair/Poor Health Days) by state
    dcc.Graph(
            id='health_trends_states_map',
            figure=datavis.healthuninsured_bubble,
            style={
                'width': '85%', 
                'border': '2px solid white', 
                'padding': '3px',
                'margin': '20px'}
        ),

    dcc.Graph(
            id='health_trends_states_map',
            figure=datavis.health_trends_states_map,
            style={
                'width': '85%', 
                'border': '2px solid white', 
                'padding': '3px',
                'margin': '20px'}
        ),
    
    # Map: Unhealthy Days by state
    dcc.Graph(
            id='unhealthy_map',
            figure=datavis.unhealthy_map,
            style={
                'width': '85%', 
                'border': '2px solid white', 
                'padding': '3px',
                'margin': '20px'}
        ),

    # Map: 2+ Chronic Conditions by state
    dcc.Graph(
            id='multi_chronic_map',
            figure=datavis.multi_chronic_map,
            style={
                'width': '85%', 
                'border': '2px solid white', 
                'padding': '3px',
                'margin': '20px'}
        ),

    # Map: Obesity by state
    dcc.Graph(
            id='obesity_map',
            figure=datavis.obesity_map,
            style={
                'width': '85%', 
                'border': '2px solid white', 
                'padding': '3px',
                'margin': '20px'}
        ),

    html.Div('The maps above show the prevalence of various indicators of health in a given state. When reading these maps, one things stood out to us: West Virginia appeared to be an unhealthy outlier in all of them.', 
            style={
                'width': '85%', 
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        ),

    html.Div('We wondered if this might be the result of West Virginia having a particularly high level of wealth inequality (as measured by the GINI index), or if there were more people uninsured there per capita than elsewhere. As the maps below show, this is not the case.', 
            style={
                'width': '85%', 
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        ),
    
    # Map: GINI index by state
    dcc.Graph(
            id='gini_map',
            figure=datavis.gini_map,
            style={
                'width': '85%', 
                'border': '2px solid white', 
                'padding': '3px',
                'margin': '20px'}
        ),
    
    # Map: Uninsured by state
    dcc.Graph(
            id='uninsured_map',
            figure=datavis.uninsured_map,
            style={
                'width': '85%', 
                'border': '2px solid white', 
                'padding': '3px',
                'margin': '20px'}
        ),
    
    html.Div('So, what\'s going on in West Virginia?', 
            style={
                'width': '85%', 
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        )
    ])

    elif tab == 'tab-2-wv':
        return html.Div([
            html.Div('Examining an Outlier: West Virginia', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '125%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px 0",
                'font-style': 'italic'
            }
        ),

     html.Div('Our intial exploratory data analysis represented in the first tab (\"National Trends\") highlighted West Virginia as an outlier in bad health. It seemed to have particularly bad outcomes relative to obesity, unhealthy recent days, self-reporting of fair or poor health status, and the proportion of their population who is living with two or more chronic health conditions.', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        ),
    
    html.Div('We wanted to see what else the data from the CDC\'s Chronic Disease Indicators dataset might tell us. First, how does West Virginia compare to the US national average among health-related outcomes and practices?', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        ),
    
    # Bar chart: West Virginia vs US
    dcc.Graph(
        id='wv_vs_us_comp',
        figure=datavis.wv_vs_us_comp_dot,
        style={
            'width': '85%', 
            'height': '900px',
            'border': '2px solid white', 
            'padding': '3px',
            "margin-left": "20px",
            "margin": "20px 0"}
    ),

    html.Div('From this chart we can see that West Virginia displays worse outcomes and behaviors in nearly all included categories here.', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        ),
    
    html.Div('We might break down some of West Virginia\'s health outcomes or behaviors by various demographic groups (e.g. by race/ethnicity, sex, or age), to see where some of the greatest disparities lie.', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        ),

    html.Div('Below, we compare health outcomes in 2022 among the US and West Virginia with corresponding behavioral data from 2019. This is partially because of data availability, but is also based on the premise that health-related behaviors are understood to have long-term effects, so looking at outcomes a few years after examining aggregate behavior may be appropriate.', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        ),

    # Bar chart: West Virginia vs US -- outcomes by race
    dcc.Graph(
        id='wv_vs_us_byrace_outcomes_figure',
        figure=datavis.wv_vs_us_byrace_outcomes_figure,
        style={
            'width': '85%', 
            #'height': '900px',
            'border': '2px solid white', 
            'padding': '3px',
            "margin-left": "20px",
            "margin": "20px 0"}
    ),

    # Bar chart: West Virginia vs US -- behaviors by race
    dcc.Graph(
        id='wv_vs_us_byrace_behavior_figure',
        figure=datavis.wv_vs_us_byrace_behavior_figure,
        style={
            'width': '85%', 
            #'height': '900px',
            'border': '2px solid white', 
            'padding': '3px',
            "margin-left": "20px",
            "margin": "20px 0"}
    ),

    html.Div('Note here that some categories of race/ethnicity are missing. In West Virginia in 2022, the outcomes data only includes prevalences for three races (White, non-Hispanic; Black, non-Hispanic; Multi-racial, non-Hispanic) for two of the questions, while the third question, about obesity, includes a fourth group (Hispanic). There are national prevalence measures for seven race/ethnicities, represented in the US data.', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        ),

        html.Div('In the behavior data, we see categories missing from several of the plots.', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        ),

    # Bar chart: West Virginia vs US -- outcomes by sex
    dcc.Graph(
        id='wv_vs_us_outcomes_bysex_figure',
        figure=datavis.wv_vs_us_outcomes_bysex_figure,
        style={
            'width': '85%', 
            #'height': '900px',
            'border': '2px solid white', 
            'padding': '3px',
            "margin-left": "20px",
            "margin": "20px 0"}
    ),

    html.Div('The gender-related data suggest two phenomena: First, that across these three questions, West Virginia has a larger proportion of unhealthy people than the national proportion, both men and women. And second, both nationally and in West Virginia, women appear to be systematically more likely to be experiencing chronic health problems than men.', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        ),

    # Bar chart: West Virginia vs US -- behavior by sex
    dcc.Graph(
        id='wv_vs_us_behavior_bysex_figure',
        figure=datavis.wv_vs_us_behavior_bysex_figure,
        style={
            'width': '85%', 
            #'height': '900px',
            'border': '2px solid white', 
            'padding': '3px',
            "margin-left": "20px",
            "margin": "20px 0"}
    ),

    html.Div('Unfortunately, not all years had data for all relevant questions. The study did not have adequate responses to the two other questions to include the data for 2022.', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        ),

    # Bar chart: West Virginia vs US -- outcomes by age
    dcc.Graph(
        id='wv_vs_us_outcomes_byage_figure',
        figure=datavis.wv_vs_us_outcomes_byage_figure,
        style={
            'width': '85%', 
            #'height': '900px',
            'border': '2px solid white', 
            'padding': '3px',
            "margin-left": "20px",
            "margin": "20px 0"}
    ),

    # Bar chart: West Virginia vs US -- behavior by age
    dcc.Graph(
        id='wv_vs_us_behavior_byage_figure',
        figure=datavis.wv_vs_us_behavior_byage_figure,
        style={
            'width': '85%', 
            #'height': '900px',
            'border': '2px solid white', 
            'padding': '3px',
            "margin-left": "20px",
            "margin": "20px 0"}
    ),

    html.Div('Comparing the data about obesity as it relates to age group, there is not a clear story emerging. One notable finding is that older West Virginians (aged 45-64) substantially outperform the national average on aerobic physical activity. One policy-related takeaway could be to key in on obesity trends in West Virginia. Both the youngest age group and the middle group see a much higher prevalence of obesity than the national prevalence.', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px"
            }
        )
    ])

    elif tab == 'tab-3-pred':
        return html.Div([
        html.Div('Machine Learning Prediction Model', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '125%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px 0",
                'font-style': 'italic'
            }
        ),

        html.Div('Below is an interactive model that will predict health outcomes.', 
                style={
                    'color': THEME['white'], 
                    'backgroundColor': THEME['bgcol'],
                    'font-family': 'Helvetica',
                    'font-size': '100%',
                    'font-weight': 'Normal',
                    "margin-left": "20px",
                    "margin": "20px"
                }
            ),
        html.H1("Health Prediction App", 
            style={
                    'color': THEME['white'], 
                    'backgroundColor': THEME['bgcol'],
                    'font-family': 'Helvetica',
                    'font-size': '125%',
                    'font-weight': 'Normal',
                    "margin-left": "20px",
                    "margin": "20px 0",
                    'font-style': 'italic'
                }),
        html.Label("Select Model",
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px 0",
                'font-style': 'italic'
            }),
        dcc.Dropdown(
            id='model-dropdown',
            options=model_options,
            value='xgb',  # Default value
            style={'width': '200px'}
        ),
        html.Label("Select Prediction Year",
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px 0",
                'font-style': 'italic'
            }),
        dcc.Input(id='year-input', 
                  type='number', 
                  value=2035
                  # style={'width': '200px'}
                  ),
        html.Label("Select Location",
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                # "margin-left": "20px",
                # "margin": "20px 0",
                'font-style': 'italic'
            }),
        dcc.Dropdown(
            id='location-dropdown',
            options=[{'label': state, 'value': state} for state in states],
            value='AL',  # Default value
            style={'width': '200px'}
        ),
        html.Label("Select Demographic Category",
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px 0",
                'font-style': 'italic'
            }),
        dcc.Dropdown(
            id='category-dropdown',
            options=categories,
            value='Sex',  # Default value
            style={'width': '200px'}
        ),
        html.Label("Select Demographic Value",
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px 0",
                'font-style': 'italic'
            }),
        dcc.Dropdown(
            id='demographic-dropdown',
            value='Female',  # Default value
            style={"width": "200px"},
        ),
        html.Label("Select Group Value",
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px 0",
                'font-style': 'italic'
            }),
        dcc.Dropdown(
            id='sub-demographic-dropdown',
            value=[],  # Default value, initially empty
            style={'width': '200px'}
        ),
        html.Button('Predict', id='predict-button'),
        html.Div(id='prediction-output',
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '100%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px 0",
                'font-style': 'italic'
            })
], style={
            'width': '85%', 
            'border': '2px solid white', 
            'padding': '3px',
            "margin-left": "20px",
            "margin": "20px 0"
            }

)



    elif tab == 'tab-4-methods':
        return html.Div([
        html.Div('Methodology', 
            style={
                'color': THEME['white'], 
                'backgroundColor': THEME['bgcol'],
                'font-family': 'Helvetica',
                'font-size': '125%',
                'font-weight': 'Normal',
                "margin-left": "20px",
                "margin": "20px 0",
                'font-style': 'italic'
            }
        ),

        html.Div('For our analysis, we began by accessing the US Centers for Disease Control and Prevention (CDC) Chronic Disease Indicators dataset, encompassing a total of 299,503 entries. Focusing on state-level health status and physical activity, we subsetted the dataset to include entries pertinent to these parameters, resulting in 14,076 entries. To ensure data quality, we further refined the dataset by excluding entries with missing values, particularly those related to race/ethnicity data which contained the most missingness, culminating in a final sample size of 10,627 entries for thorough analysis. Additionally, we accessed uninsured population rates and Gini indices for each state in 2022 from PolicyMap, which utilizes U.S. Census data. These supplementary variables will augment our analysis, providing insights into the socioeconomic determinants of health and potential disparities across states. ', 
                style={
                    'color': THEME['white'], 
                    'backgroundColor': THEME['bgcol'],
                    'font-family': 'Helvetica',
                    'font-size': '100%',
                    'font-weight': 'Normal',
                    "margin-left": "20px",
                    "margin": "20px"
                }
            ),

        html.Div('WORKS CITED', 
                style={
                    'color': THEME['white'], 
                    'backgroundColor': THEME['bgcol'],
                    'font-family': 'Helvetica',
                    'font-size': '100%',
                    'font-weight': 'Normal',
                    "margin-left": "20px",
                    "margin": "20px"
                }
            ),

        html.Div('Urban Institute. (2024). Guide to equity for the uninsured.', 
                style={
                    'color': THEME['white'], 
                    'backgroundColor': THEME['bgcol'],
                    'font-family': 'Helvetica',
                    'font-size': '100%',
                    'font-weight': 'Normal',
                    "margin-left": "20px",
                    "margin": "20px"
                }
            ), 
            html.Div('Griffith KN, Bor JH. Changes in Health Care Access, Behaviors, and Self-reported Health Among Low-income US Adults Through the Fourth Year of the Affordable Care Act. Med Care. 2020 Jun;58(6):574-578.', 
                style={
                    'color': THEME['white'], 
                    'backgroundColor': THEME['bgcol'],
                    'font-family': 'Helvetica',
                    'font-size': '100%',
                    'font-weight': 'Normal',
                    "margin-left": "20px",
                    "margin": "20px"
                }
            )
    ])


## Define callbacks for ML charts
# Define callback to update sub-category dropdown options based on selected category
@app.callback(
    Output('sub-demographic-dropdown', 'options'),
    [Input('category-dropdown', 'value')]
)
def update_sub_demographic_options(selected_category):
    if selected_category == 'Age':
        return age_options
    elif selected_category == 'Sex':
        return sex_options
    elif selected_category == 'Race/Ethnicity':
        return race_ethnicity_options
    else:
        return []

# Define callback to make predictions based on user input and selected model
@app.callback(
    Output('prediction-output', 'children'),
    [Input('predict-button', 'n_clicks')],
    [State('model-dropdown', 'value'),
     State('year-input', 'value'),
     State('location-dropdown', 'value'),
     State('category-dropdown', 'value'),
     State('demographic-dropdown', 'value'),
     State('sub-demographic-dropdown', 'value')]
)
def predict_health_status(n_clicks, selected_model, prediction_year, prediction_location, prediction_category, prediction_demographic, prediction_sub_demographic):
    if n_clicks is None:
        return ''

    # Construct input data for prediction
    input_data = pd.DataFrame({
        'yearstart': [prediction_year],
        'igini': [0.9],  # Example value, replace with actual value if needed
        'locationabbr': [prediction_location],
        'stratificationcategory1': [prediction_category],
        'stratification1': [prediction_demographic],
        'stratification2': [prediction_sub_demographic]  # Include sub-demographic in input data
    })

    # Select the appropriate model based on user's choice
    if selected_model == 'linear':
        model = linear_model
    elif selected_model == 'random_forest':
        model = rf_model
    else:
        model = xgb_model

    # Make prediction using the selected model
    prediction = model.predict(input_data)

    return f"Predicted prevalence of self-reported fair/poor health outcomes among {prediction_sub_demographic} in {prediction_location} in {prediction_year} using {selected_model}: {prediction[0]}/%"




# # Define callback to update the subcategory dropdown based on category selection
# @app.callback(
#     Output('subcategory-dropdown-div', 'children'),
#     [Input('category-dropdown', 'value')]
# )
# def update_subcategory_dropdown(category):
#     if category == 'Age':
#         return dcc.Dropdown(
#             id='subcategory-dropdown',
#             options=age_options,
#             value='18-44'  # Default value
#         )
#     elif category == 'Sex':
#         return dcc.Dropdown(
#             id='subcategory-dropdown',
#             options=sex_options,
#             value='Female'  # Default value
#         )
#     elif category == 'Race/Ethnicity':
#         return dcc.Dropdown(
#             id='subcategory-dropdown',
#             options=race_ethnicity_options,
#             value='Hispanic'  # Default value
#         )
#     else:
#         return html.Div()

# # Define callback to update the chart based on user input
# @app.callback(
#     Output('line-chart', 'figure'),
#     [Input('model-dropdown', 'value'),
#      Input('state-dropdown', 'value'),
#      Input('category-dropdown', 'value'),
#      Input('subcategory-dropdown', 'value')]
# )

# def update_chart(model_name, state, category, subcategory):

#     # Initialize lists to store data for plotting
#     years = np.arange(2023, 2051)
#     years_list = []
#     predictions_list = []

#     # Iterate over years and predict for each year
#     for year in years:
#         # Construct input data for the current year
#         input_data = {
#             'yearstart': [year],
#             'igini': [0.9],
#             'locationabbr': [state],
#             'stratificationcategory1': [category],
#             'stratification1': [subcategory]
#         }

#         # Apply OneHotEncoder to categorical features
#         preprocessor = ColumnTransformer(transformers=[
#                         ('onehot', OneHotEncoder(drop='first', handle_unknown='ignore', sparse_output=False), categorical_inputs)
#                         ], 
#                         remainder='drop')

#         # Create a prediction object from user inputs
#         X_predict = pd.DataFrame({
#             'yearstart': [year],
#             'igini': [0.9],
#             'locationabbr': [state],
#             'stratificationcategory1': [category],
#             'stratification1': [subcategory]
#         })

#         # Calculate predicted values based on user inputs
#         predicted_values = []

#         if model_name == 'linear':
#             model = linear_model
#         elif model_name == 'rf':
#             model = rf_model
#         elif model_name == 'xgb':
#             model = xgb_model
#         else:
#             raise ValueError("Invalid model selected")

#         # Predict 'datavalue' based on the selected inputs
#         predicted_values = model.predict(X_predict)

#         # Create the chart data for subcategories
#         chart_data = []
#         for subcategory in subcategories.get(category, ['Overall']):
#             # Generate predicted values for each subcategory (replace with actual code)
#             # Example: predicted_values_subcategory = model.predict(inputs_subcategory)
#             predicted_values_subcategory = model.predict(inputs_subcategory)

#             data = {
#                 'x': years,
#                 'y': predicted_values_subcategory,
#                 'type': 'line',
#                 'name': subcategory
#             }
#             chart_data.append(data)

#         return {'data': chart_data}


if __name__ == '__main__':
    app.run_server(debug=True, port=8080)