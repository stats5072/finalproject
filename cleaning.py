import numpy as np
import pandas as pd
pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
import os

def open_csv(relative_path,header):
    current_directory = os.getcwd()

    absolute_path = os.path.join(current_directory, relative_path)

    csv_data = pd.read_csv(absolute_path,header = header)

    return csv_data

df = open_csv(relative_path="data/USChronicDiseaseIndicators.csv",header=0)

gini_index = open_csv(relative_path="data/Gini_index.csv",header=1)

uninsured = open_csv(relative_path="data/Uninsured_rates.csv",header=1)

# Cleaning US Chronic disease data frame 

## Making variable names lower case and dropping columns that are largely missing
df.columns = df.columns.str.lower()

df = df.dropna(axis=1, how='all').drop(['datavaluefootnotesymbol', 'datavaluefootnote'], axis=1)

# Cleaning Gini Index data 
gini_index.columns = gini_index.columns.str.lower()

gini_index = gini_index[['sitsinstate', 'igini']]

# Cleaning uninsured rates
uninsured.columns = uninsured.columns.str.lower()

uninsured = uninsured[['sitsinstate', 'ppopwoins']]

# Appending US Chronic disease data frame with additional variables

df_combined = pd.merge(df, gini_index[['sitsinstate','igini']], how='left', left_on='locationabbr', right_on='sitsinstate')

df_combined = pd.merge(df_combined, uninsured[['sitsinstate','ppopwoins']], how='left', left_on='locationabbr', right_on='sitsinstate')

df_combined = df_combined.drop(['sitsinstate_x','sitsinstate_y'],axis = 1)

## Dropping the US territories because we don't have that data in the additional data sets
df_combined = df_combined[~df_combined['locationabbr'].isin(['GU', 'VI','PR'])]

## We want to look at only topics related to health (Health Status and Nutrition, Physical Activity, and Weight Status)

healthtopics = df_combined[df_combined['topic'].isin(['Nutrition, Physical Activity, and Weight Status', 'Health Status'])]

# Filter state-level data and specific questions
healthtopics_states = healthtopics[
    (healthtopics['locationabbr'] != 'US') &
    (healthtopics['question'].isin([
        'Frequent physical distress among adults',
        'Average recent physically unhealthy days among adults',
        'Fair or poor self-rated health status among adults'
    ]))
]

complete_df = healthtopics_states[~pd.isna(healthtopics_states['datavalue'])]